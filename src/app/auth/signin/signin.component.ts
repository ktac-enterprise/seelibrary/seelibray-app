import { Component, OnInit } from '@angular/core';
import {SocialAuthService} from "../../services/social-auth.service";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  constructor(private autSocial: SocialAuthService) { }

  ngOnInit(): void {
  }

  onGoogleLogin() {
    this.autSocial.googleLogin().then(r => console.log(r)).catch(reason => console.log(reason));
  }

  onFaceBookLogin(){
    this.autSocial.facebookLogin().then(value => console.log(value)).catch(reason => console.log(reason));
  }
}
